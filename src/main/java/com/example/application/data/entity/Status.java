package com.example.application.data.entity;

import com.example.application.data.AbstractEntity;
import javax.persistence.Entity;

@Entity
public class Status extends AbstractEntity {
    private String name;

    public Status() {

    }

    public Status(String name) {
        this.name = name;
    }

    public String getStatusName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	@Override
	public String getCompanyName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLastName() {
		// TODO Auto-generated method stub
		return null;
	}
}
