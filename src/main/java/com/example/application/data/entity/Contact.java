package com.example.application.data.entity;

import com.example.application.data.AbstractEntity;
import com.example.application.data.service.CrmService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vaadin.flow.function.ValueProvider;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Contact extends AbstractEntity {

	
    public Contact(@NotNull Company company) {
		super();
		this.company = company;
	}

	public Contact() {
		super();
	}

	public Contact(@NotEmpty String firstName, @NotEmpty String lastName, @NotNull Company company,
			@NotNull Status status, @Email @NotEmpty String email, LocalDate dateCreated) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = company;
		this.status = status;
		this.email = email;
		this.dateCreated = dateCreated;
	}

	@NotEmpty
    private String firstName = "";

    @NotEmpty
    private String lastName = "";

    @ManyToOne
    @JoinColumn(name = "company_id")
    @NotNull
    @JsonIgnoreProperties({"employees"})
    private Company company;

    @NotNull
    @ManyToOne
    private Status status;

    @Email
    @NotEmpty
    private String email = "";

    private LocalDate dateCreated = null;

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public LocalDate getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDate dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public String getCompanyName() {
		// TODO Auto-generated method stub
		return null;
	}
}
