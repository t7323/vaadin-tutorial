package com.example.application.views.list;

import java.util.List;
import java.util.stream.Stream;

import com.example.application.data.entity.Company;
import com.example.application.data.entity.Contact;
import com.example.application.data.entity.Status;
import com.example.application.data.service.CrmService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

public class ContactDetailsFormLayout extends FormLayout {

	TextField firstName = new TextField("First Name");
	TextField lastName = new TextField("Last Name");
	EmailField email = new EmailField("Email");
	DatePicker dateCreated = new DatePicker("Date Created");	
	ComboBox<Status> status = new ComboBox<>("Status");
	ComboBox<Company> company = new ComboBox<>("Company");
	
	Binder<Contact> binder = new BeanValidationBinder<>(Contact.class);
	
	private Contact contact;

	Button save = new Button("Save");
	Button delete = new Button("Delete");
	Button close = new Button("Close");
	
	public ContactDetailsFormLayout(List<Company> companies, List<Status> statuses) {
		addClassName("contact-details-form");
		binder.bindInstanceFields(this);
		company.setItems(companies);
		company.setItemLabelGenerator(Company::getCompanyName);
		status.setItems(statuses);
		status.setItemLabelGenerator(Status::getStatusName);
		
		add(firstName, lastName, email, dateCreated, company, status, createButtonsLayout());
		
		setResponsiveSteps(new ResponsiveStep("0", 7));
		setColspan(firstName, 3);
		setColspan(lastName, 3);
		setColspan(email, 3);
		setColspan(dateCreated, 3);
		setColspan(status, 3);
		setColspan(company, 3);
	}
	
	public void setContact(Contact contact) {
		this.contact = contact;
		binder.readBean(contact);
			
	}
	
	private Component createButtonsLayout() {
		save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
		close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		
		save.addClickShortcut(Key.ENTER);
		close.addClickShortcut(Key.ESCAPE);
		
		save.addClickListener(event -> validateAndSave());
		delete.addClickListener(event -> fireEvent(new DeleteEvent(this, contact)));
		close.addClickListener(event -> fireEvent(new CloseEvent(this)));
		
		binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
		return new HorizontalLayout(save, delete, close);
	}
	
	private void validateAndSave() {
		try {
			binder.writeBean(contact);
			fireEvent(new SaveEvent(this, contact));
		} catch (ValidationException e){
			e.printStackTrace();
		}
	}
	
	//events
	public static abstract class ContactDetailsFormEvent extends ComponentEvent<ContactDetailsFormLayout> {
		
		private Contact contact;
		
		protected ContactDetailsFormEvent(ContactDetailsFormLayout source, Contact contact) {
			super(source, false);
			this.contact = contact;	
		}
		
		public Contact getContact() {
			return contact;
		}
	}
	
	public static class SaveEvent extends ContactDetailsFormEvent {
		SaveEvent(ContactDetailsFormLayout source, Contact contact) {
			super(source, contact);
		}
	}
	
	public static class DeleteEvent extends ContactDetailsFormEvent {
		DeleteEvent(ContactDetailsFormLayout source, Contact contact) {
			super(source, contact);
		}
	}
	
	public static class CloseEvent extends ContactDetailsFormEvent {
		CloseEvent(ContactDetailsFormLayout source) {
			super(source, null);
		}
	}
	
	public <T extends ComponentEvent<?>> Registration addListener(
			Class<T> eventType,
			ComponentEventListener<T> listener) {
		return getEventBus().addListener(eventType, listener);
	}
	
}
