package com.example.application.views.list;                                                                                  
                                                                                                                             
import java.util.List;                                                                                                       
import java.util.stream.Stream;                                                                                              
                                                                                                                             
import org.springframework.context.annotation.Scope;                                                                         
import org.springframework.stereotype.Component;                                                                             
                                                                                                                             
import com.example.application.data.entity.Company;                                                                          
import com.example.application.data.entity.Contact;                                                                          
import com.example.application.data.entity.Status;                                                                           
import com.example.application.data.service.CrmService;                                                                      
                                                                                                                             
import com.vaadin.flow.component.button.Button;                                                                              
import com.vaadin.flow.component.formlayout.FormLayout;                                                                      
import com.vaadin.flow.component.grid.Grid;                                                                                  
import com.vaadin.flow.component.grid.GridVariant;                                                                           
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;                                                             
import com.vaadin.flow.component.orderedlayout.VerticalLayout;                                                               
import com.vaadin.flow.component.textfield.TextField;                                                                        
import com.vaadin.flow.data.renderer.ComponentRenderer;                                                                      
import com.vaadin.flow.data.renderer.LocalDateRenderer;                                                                      
import com.vaadin.flow.data.renderer.Renderer;                                                                               
import com.vaadin.flow.data.renderer.TemplateRenderer;                                                                       
import com.vaadin.flow.data.value.ValueChangeMode;                                                                           
import com.vaadin.flow.router.PageTitle;                                                                                     
import com.vaadin.flow.router.Route;                                                                                         
                                                                                                                             
@Component                                                                                                                   
@Scope("prototype")                                                                                                          
@Route(value = "", layout = MainLayout.class)                                                                                
@PageTitle("Contacts | Vaadin CRM")                                                                                          
public class ListView extends VerticalLayout {                                                                               
                                                                                                                             
	//Grid<Contact> grid = new Grid<>(Contact.class, false);      
	TextField filterText = new TextField();                                                                                  
	//ContactForm form;                                                                                                      
	ContactDetailsFormLayout form;      
	CrmService service;
	ContactTreeGrid grid; 
                                                                                                                 
	public ListView(CrmService service) {   
		this.service = service;

		addClassName("list-view");                                                                                           
		setSizeFull();                                                                                                       
		configureGrid();

		configureForm();                                                                                                     
		add(getToolbar(), getContent());                                                                                     
		updateList();                                                                                                        
		closeEditor();                                                                                                       
	}                                                                                                                        
                                                                                                                             
	private void configureGrid() {	
		addClassNames("contact-tree-grid");
		setSizeFull();
		grid = new ContactTreeGrid(service.findAllContacts(filterText.getValue()), service.findAllCompanies());
		this.add(grid);

	//	                                                                                                                     
	//	grid.addClassNames("contact-grid");                                                                                  
	//	grid.setSizeFull();                                                                                                  
	//	grid.addColumn(contact -> contact.getId()).setHeader("Id").setSortable(true);                                        
	//	grid.addColumn(contact -> contact.getFirstName()).setHeader("First Name").setSortable(true);                         
	//	grid.addColumn(contact -> contact.getLastName()).setHeader("Last Name").setSortable(true);                           
	//	grid.addColumn(contact -> contact.getEmail()).setHeader("Email").setSortable(true);                                  
	//	grid.addColumn(new LocalDateRenderer<>(contact -> contact.getDateCreated(), "dd/MM/yyyy")).setHeader("Date Created");
	//	grid.addColumn(contact -> contact.getStatus().getName()).setHeader("Status");                                        
	//	grid.addColumn(contact -> contact.getCompany().getName()).setHeader("Company");                                      
    //                                                                                                                         
	//	grid.addComponentColumn(contact -> {                                                                                 
	//		Button editButton = new Button("Edit");                                                                          
	//		editButton.addClickListener(click -> {                                                                           
	//			editContact(contact);                                                                                        
	//			                                                                                                             
    //        });                                                                                                              
    //        return editButton;                                                                                               
    //    });                                                                                                                  
    //                                                                                                                         
    //    List<Contact> contacts = service.findAllContacts(filterText.getValue());                                             
    //    grid.setItems(contacts);                                                                                             
    //                                                                                                                         
	//	grid.setDetailsVisibleOnClick(false);                                                                                
    //    grid.setItemDetailsRenderer(createContactDetailsRenderer());                                                         
    //                                                                                                                         
    //                                                                                                                         
    //                                                                                                                         
    //    grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);                                                                 
    //                                                                                                                         
	//	                                                                                                                     
	//	grid.getColumns().forEach(col -> col.setAutoWidth(true));		
	}                                                                                                                        
	                                                                                                                         
	private ComponentRenderer<ContactDetailsFormLayout, Contact> createContactDetailsRenderer() {                            
		return new ComponentRenderer<>(contact -> {                                                                          
					ContactDetailsFormLayout layout = new ContactDetailsFormLayout(service.findAllCompanies(), service.findAllStatuses());
					layout.setContact(contact);                                                                              
					return layout;                                                                                           
				});                                                                                                          
		                                                                                                                     
				//(layout) -> new ContactDetailsFormLayout(companies, statuses),                                             
				//(layout, contact) -> layout.setContact(contact));                                                          
				                                                                                                             
	}                                                                                                                        
	                                                                                                                         
	/* layout component as inner class                                                                                       
	 *                                                                                                                       
	 */                                                                                                                      
	//private static class ContactDetailsFormLayout extends FormLayout {                                                     
	//	private final TextField firstNameField = new TextField("First Name");                                                
	//	private final TextField lastNameField = new TextField("Last Name");                                                  
	//	private final TextField emailField = new TextField("Email");                                                         
	//	private final TextField statusField = new TextField("Status");                                                       
	//	private final TextField companyField = new TextField("Company");                                                     
	//	                                                                                                                     
	//	public ContactDetailsFormLayout() {                                                                                  
	//		Stream.of(firstNameField, lastNameField, emailField, statusField, companyField).forEach(field -> {               
	//			field.setReadOnly(false);                                                                                    
	//			add(field);                                                                                                  
	//		});                                                                                                              
	//		                                                                                                                 
	//		setResponsiveSteps(new ResponsiveStep("0", 5));                                                                  
	//		setColspan(firstNameField, 3);                                                                                   
	//		setColspan(lastNameField, 3);                                                                                    
	//		setColspan(emailField, 3);                                                                                       
	//		setColspan(statusField, 3);                                                                                      
	//		setColspan(companyField, 3);                                                                                     
	//	}                                                                                                                    
	//	                                                                                                                     
	//	public void setContact(Contact contact) {                                                                            
	//		firstNameField.setValue(contact.getFirstName());                                                                 
	//		lastNameField.setValue(contact.getLastName());                                                                   
	//		emailField.setValue(contact.getEmail());                                                                         
	//		statusField.setValue(contact.getStatus().getName());                                                             
	//		companyField.setValue(contact.getCompany().getName());                                                           
	//	}                                                                                                                    
	//	                                                                                                                     
    //                                                                                                                       
	//}                                                                                                                      
                                                                                                                             
	private void closeEditor() {                                                                                             
		form.setContact(null);                                                                                               
		form.setVisible(false);                                                                                              
		removeClassName("editing");                                                                                          
	}                                                                                                                        
                                                                                                                             
	private void updateList() {                                                                                              
		//grid.setItems(service.findAllContacts(filterText.getValue()));                                                       
	}                                                                                                                        
                                                                                                                             
	private HorizontalLayout getContent() {                                                                                  
		HorizontalLayout content = new HorizontalLayout(grid);                                                               
		content.setFlexGrow(2, grid);                                                                                        
		content.setFlexGrow(1, form);                                                                                      
		content.addClassNames("content");                                                                                    
		content.setSizeFull();                                                                                               
		return content;                                                                                                      
	}                                                                                                                        
                                                                                                                             
	private void configureForm() {                                                                                           
		//form = new ContactForm(service.findAllCompanies(), service.findAllStatuses());                                     
		form = new ContactDetailsFormLayout(service.findAllCompanies(), service.findAllStatuses());                          
		form.setWidth("25em");                                                                                               
		form.addListener(ContactDetailsFormLayout.SaveEvent.class, this::saveContact);                                       
		form.addListener(ContactDetailsFormLayout.DeleteEvent.class, this::deleteContact);                                   
		form.addListener(ContactDetailsFormLayout.CloseEvent.class, e -> closeEditor());                                     
	}                                                                                                                        
	                                                                                                                         
	private void saveContact(ContactDetailsFormLayout.SaveEvent event) {                                                     
		service.saveContact(event.getContact());                                                                             
		updateList();                                                                                                        
		closeEditor();                                                                                                       
	}                                                                                                                        
	                                                                                                                         
	private void deleteContact(ContactDetailsFormLayout.DeleteEvent event) {                                                 
		service.deleteContact(event.getContact());                                                                           
		updateList();                                                                                                        
		closeEditor();                                                                                                       
	}                                                                                                                        
                                                                                                                             
	private void editContact(Contact contact) {                                                                              
		if (contact == null) {                                                                                               
			closeEditor();                                                                                                   
		} else {                                                                                                             
			//grid.setDetailsVisible(contact, !grid.isDetailsVisible(contact));                                                
			form.setContact(contact);                                                                                        
			form.setVisible(true);                                                                                           
			addClassName("editing");                                                                                         
		}                                                                                                                    
	}                                                                                                                        
                                                                                                                             
	private HorizontalLayout getToolbar() {                                                                                  
		filterText.setPlaceholder("Filter by name...");                                                                      
		filterText.setClearButtonVisible(true);                                                                              
		filterText.setValueChangeMode(ValueChangeMode.LAZY);                                                                 
		filterText.addValueChangeListener(e -> updateList());                                                                
		                                                                                                                     
		Button addContactButton = new Button("Add contact");                                                                 
		addContactButton.addClickListener(click -> addContact());                                                            
		                                                                                                                     
		HorizontalLayout toolbar = new HorizontalLayout(filterText, addContactButton);                                       
		toolbar.addClassName("toolbar");                                                                                     
		return toolbar;                                                                                                      
	}                                                                                                                        
                                                                                                                             
	private void addContact() {                                                                                              
		//grid.asSingleSelect().clear();                                                                                       
		editContact(new Contact());                                                                                          
	}                                                                                                                        
	                                                                                                                         
                                                                                                                             
}                                                                                                                            
                                                                                                                             