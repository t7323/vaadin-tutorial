package com.example.application.serurity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	public static final String LOGIN_PROCESSING_URL = "/login";
	public static final String LOGIN_FAILURE_URL = "/login?error";
	public static final String LOGIN_URL = "/login";
	public static final String LOGOUT_SUCCESS_URL = "/login";
	
	/**
	   * Require login to access internal pages and configure login form.
	   */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		//vaadin handles csrf internally
		http.csrf().disable()
			//register our CustomerRequestCache, which saves unauthorized access attempt
			.requestCache().requestCache(new CustomRequestCache())
			//restrict access to the application
			.and().authorizeRequests()
			//allow all vaadin internal requests
			.requestMatchers(SecurityUtils::isFrameworkInternalRequest).permitAll()
			//allow all requests by logged-in users
			.anyRequest().authenticated()
			//configure login
			.and().formLogin()
			.loginPage(LOGIN_URL).permitAll()
			.loginProcessingUrl(LOGIN_PROCESSING_URL)
			.failureUrl(LOGIN_FAILURE_URL)
			//configure logout
			.and().logout().logoutSuccessUrl(LOGOUT_SUCCESS_URL);
	}
	
	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		
		UserDetails user = User.withUsername("user")
				.password("{noop}userpass")
				.roles("USER")
				.build();
		
		return new InMemoryUserDetailsManager(user);
	}
	
	/**
	   * Allows access to static resources, bypassing Spring Security.
	   */
	@Override
	public void configure(WebSecurity web) {
		
		web.ignoring().antMatchers(
				//client side JS
				"/VAADIN/**",
				//standard favicon uri
				"/favicon.ico",
				//robots exclusion standard
				"/robots.txt",
				//web application manifest
				"/manifest.webmanifest",
				"/sw.js",
				"/offline.html",
				//icons and images
				"/icons/**",
				"/images/**",
				"/styles/**",
				//development mode h2  debugging console
				"/h2-console/**");
	}
}
