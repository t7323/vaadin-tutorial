package com.example.application.it;

import org.junit.Assert;
import org.junit.Test;

import com.example.application.it.elements.LoginViewElement;
import com.vaadin.flow.component.login.testbench.LoginFormElement;

public class LoginIT extends AbstractTest {

	public LoginIT() {
		super("");
	}
	
	@Test
	public void loginAsValidUserSucceeds() {
		
		/**
		 * BEFORE CREATING LoginViewElement Class
		 * 
		//find LoginForm used on the page
		LoginFormElement form = $(LoginFormElement.class).first();
		//enter credentials and log in
		form.getUsernameField().setValue("user");
		form.getPasswordField().setValue("userpass");
		form.getSubmitButton().click();
		//ensure login form is no longer visible
		Assert.assertFalse($(LoginFormElement.class).exists());
		**/
		
		LoginViewElement loginView = $(LoginViewElement.class).onPage().first();
		Assert.assertTrue(loginView.login("user", "userpass"));
	}
	
	@Test
	public void loginAsInvalidUserFails() {
		LoginViewElement loginView = $(LoginViewElement.class).onPage().first();
		Assert.assertFalse(loginView.login("user", "userpass"));
	}
	
}
